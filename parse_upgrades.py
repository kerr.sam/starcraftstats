
import os
import xml.etree.ElementTree as ET
import collections
import json

directory = "C:\\Users\\kerrs\\OneDrive\\Desktop\\Balance"
results = collections.defaultdict(dict)

def do_upgrades(root):
    returnable = collections.defaultdict(dict)

    researches = root.findall('researches')
    if researches == []:
        return returnable

    upgrades = []
    for x in researches:
        upgrades.extend(x)

    for u in upgrades:
        print(u.attrib['id'])
        cost = u.findall('cost')
        for c in cost:
            for a in c.attrib:
                returnable[u.attrib['id']][a] = c.attrib[a]
            print(c.attrib)

    return returnable

def do_addons(root):
    returnable = collections.defaultdict(dict)

    builds = root.findall('builds')
    if builds == []:
        return returnable

    extensions = []
    for x in builds:
        extensions.extend(x)

    for e in extensions:
        print(e.attrib['id'])
        cost = e.findall('cost')
        for c in cost:
            for a in c.attrib:
                returnable[e.attrib['id']][a] = c.attrib[a]
            print(c.attrib)

    return returnable

with open("upgrades.json", "w") as f:

    for filename in os.listdir(directory):
        if filename.endswith(".xml"):

            tree = ET.parse(os.path.join(directory, filename))
            root = tree.getroot()

            results.update(do_upgrades(root))
            results.update(do_addons(root))

        else:
            continue
    f.write(json.dumps(results))
print(results)