import sys

import sc2reader

def f_to_s(frame):
    # 16 frames per second
    # 1.4 scaling factor
    # Numbers from https://github.com/ggtracker/sc2reader/blob/c2a5407cb13fc41e2b44af07ae2a7ba48ffb7695/sc2reader/resources.py#L285
    return int(frame/16/1.4)

def formatTeams(replay):
    teams = list()
    for team in replay.teams:
        players = list()
        for player in team:
            players.append("({0}) {1}".format(player.pick_race[0], player.name))
        formattedPlayers = '\n         '.join(players)
        teams.append("Team {0}:  {1}".format(team.number, formattedPlayers))
    return '\n\n'.join(teams)

def formatReplay(replay):
    return """
{filename}
--------------------------------------------
SC2 Version {release_string} - {build}
{category} Game, {start_time}
{type} on {map_name}
Length: {game_length} time {game_fps} fps
{speed}
{real_length}

{formattedTeams}
""".format(formattedTeams=formatTeams(replay), **replay.__dict__)

import os
import requests, json
import collections

def get_production(replay):
    fp_times = open('train_commands.json', 'r')
    train_times = json.load(fp_times)
    fp_times.close()

    fp_upgrades = open('upgrades.json', 'r')
    upgrades = json.load(fp_upgrades)
    fp_upgrades.close()

    types = collections.defaultdict(int)

    # Create a default dictionary for production[second][player name][event type] = production count
    production = collections.defaultdict(lambda: collections.defaultdict(lambda: collections.defaultdict(int)))
    events = replay.player[1].events
    #print(events)
    events = replay.events

    for e in events:
        if e.frame == 0:
            # Skip frame 0, since this is when units are spawned at game start
            continue

        types[type(e)] = types[type(e)]+1

        if isinstance(e, sc2reader.events.tracker.PlayerSetupEvent)\
                or isinstance(e, sc2reader.events.tracker.PlayerStatsEvent)\
                or isinstance(e, sc2reader.events.game.CameraEvent)\
                or isinstance(e, sc2reader.events.game.BasicCommandEvent)\
                or isinstance(e, sc2reader.events.game.SelectionEvent)\
                or isinstance(e, sc2reader.events.game.AddToControlGroupEvent)\
                or isinstance(e, sc2reader.events.game.GetControlGroupEvent)\
                or isinstance(e, sc2reader.events.game.TargetPointCommandEvent) \
                or isinstance(e, sc2reader.events.game.TargetUnitCommandEvent) \
                or isinstance(e, sc2reader.events.game.DataCommandEvent) \
                or isinstance(e, sc2reader.events.tracker.UnitPositionsEvent) \
                or isinstance(e, sc2reader.events.game.SetControlGroupEvent) \
                or isinstance(e, sc2reader.events.game.PlayerLeaveEvent) \
                or isinstance(e, sc2reader.events.game.UpdateTargetUnitCommandEvent) \
                or isinstance(e, sc2reader.events.tracker.UnitInitEvent) \
                or isinstance(e, sc2reader.events.tracker.UnitTypeChangeEvent) \
                or isinstance(e, sc2reader.events.tracker.UnitDiedEvent) \
                or isinstance(e, sc2reader.events.message.ChatEvent):
            # Skip these
            continue

        if isinstance(e, sc2reader.events.tracker.UnitBornEvent) and e.control_pid==0:
            # Skip non-player events
            continue

        found = False

        if isinstance(e, sc2reader.events.tracker.UpgradeCompleteEvent):
            if e.upgrade_type_name in ['SprayProtoss', 'SprayZerg', 'SprayTerran']:
                continue

            timing = f_to_s(e.frame)
            print("[*] Production event: %s @ second %d" % (e, timing))
            print("[*]   Implied start at %s seconds" % (timing-v[1]))

            # Lookup the entry
            thetime = int(upgrades[e.upgrade_type_name]['time'])
            if timing - thetime < 0:
                print("[X] Negative!")
                print(e)
            for i in range(int(thetime)):
                production[timing - i][e.player.pid][type(e)] = production[timing - i][e.player.pid][
                                                                       type(e)] + 1
            found = True

        if isinstance(e, sc2reader.events.tracker.UnitBornEvent):
            # Lookup the entry
            if e.unit.title in ["Larva", "Broodling"]:
                # Larva are born, but not produced
                continue

            for k,v in train_times.items():
                #print(e.unit.title)
                #print(dir(e.unit))
                if e.unit_type_name == v[0]:
                    timing = f_to_s(e.frame)
                    print("[*] Production event: %s @ second %d" % (e, timing))
                    print("[*]   Implied start at %s seconds" % (timing-v[1]))

                    if timing-v[1] < 0:
                        print("[X] Negative!")
                        print(e)
                    for i in range(int(v[1])):
                        production[timing-i][e.control_pid][type(e)] = production[timing-i][e.control_pid][type(e)] + 1
                    found=True

        if isinstance(e, sc2reader.events.tracker.UnitDoneEvent):
            # Lookup the entry
            for k, v in train_times.items():
                # print(e.unit.title)
                #print(dir(e))
                # print(dir(e.unit))
                if e.unit.title == v[0]:
                    timing = f_to_s(e.frame)
                    print("[*] Production event: %s @ second %d" % (e, timing))
                    print("[*]   Implied start at %s seconds" % (timing-v[1]))

                    if timing - v[1] < 0:
                        print("[X] Negative!")
                        print(e)
                    for i in range(int(v[1])):
                        production[timing - i][e.unit.owner.pid][type(e)] = production[timing - i][e.unit.owner.pid][
                                                                               type(e)] + 1
                    found = True
                    break

                if not found:
                    for i in e.unit.type_history:
                        if e.unit.type_history[i].name == v[0]:
                            timing = f_to_s(e.frame)
                            print("[*] Production event: %s @ second %d" % (e, timing))
                            print("[*]   Implied start at %s seconds" % (timing-v[1]))

                            if timing - v[1] < 0:
                                print("[X] Negative!")
                                print(e)
                            for i in range(int(v[1])):
                                production[timing - i][e.unit.owner.pid][type(e)] = \
                                production[timing - i][e.unit.owner.pid][
                                    type(e)] + 1
                            found = True

            if not found:
                timing = f_to_s(e.frame)
                print("[*] Production event: %s @ second %d" % (e, timing))

                # Lookup the entry
                print ([e.unit.type_history[x].title for x in e.unit.type_history])
                if e.unit.title not in upgrades:
                    for i in e.unit.type_history:
                        if e.unit.type_history[i].name in upgrades:
                            thetime = int(upgrades[e.unit.type_history[i].name]['time'])
                else:
                    thetime = int(upgrades[e.unit.title]['time'])


                print("[*]   Implied start at %s seconds" % (timing - thetime))

                if timing - thetime < 0:
                    print("[X] Negative!")
                    print(e)
                for i in range(int(thetime)):
                    production[timing - i][e.unit.owner.pid][type(e)] = production[timing - i][e.unit.owner.pid][
                                                                      type(e)] + 1
                found = True

        if not found:
            print("[X] Couldn't find the unit!")
            print(e)
            print(dir(e))
            print(type(e))
            # print(e.unit)
            # print(e.unit.type)
            # print(e.unit.type_history)
            # for i in e.unit.type_history:
            #     print(dir(e.unit.type_history[i]))
            #     print(e.unit.type_history[i].name)
            # print(dir(e.unit))
            # print(e.unit.title)

            #print(e.unit_type_name)
            #print("Second: %d" % e.second)
            print("Current frame %d" % e.frame)

    return production

def export_results(replay, production):
    players = [
        {
            "name": "AI (not a player)",
            "race": "AI (not a player)"
        },
        {
            "name": replay.teams[0].players[0].name,
            "race": replay.teams[0].players[0].play_race
        },
        {
            "name": replay.teams[1].players[0].name,
            "race": replay.teams[1].players[0].play_race
        }
    ]

    with open('%s.parsed.csv' % (os.path.basename(replay.filename)), 'w') as f:
        f.write("Second, Player Name, Player race, Production Type, Production Count\n")
        for i in range(len(production)):
            for v in production[i]:
                for k in production[i][v].keys():
                    data_line = "%d, %s, %s, %s, %s\n" % (i, players[v]['name'], players[v]['race'], k, production[i][v][k])
                    f.write(data_line)

def main():
    path = sys.argv[1]

    # This is where the original timing data is from
    # Other timing data is from exporting the values from inside the Starcraft 2 editor. Could also get times from the XML files there
    # r = requests.get('https://raw.githubusercontent.com/ggtracker/sc2reader/upstream/sc2reader/data/train_commands.json')
    # train_times = json.loads(r.text)

    for f in [os.path.join(path, f) for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]:
        replay = sc2reader.load_replay(f)

        print(sc2reader.constants.GAME_SPEED_FACTOR[replay.expansion][replay.speed])
        print(replay.frames/16/60/1.4)

        print(formatReplay(replay))
        print(replay.game_length)
        print(replay.real_length)

        production = get_production(replay)
        export_results(replay, production)
        break

if __name__ == '__main__':
    main()